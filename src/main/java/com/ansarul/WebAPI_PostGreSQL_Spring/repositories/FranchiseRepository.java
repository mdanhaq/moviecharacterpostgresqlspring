package com.ansarul.WebAPI_PostGreSQL_Spring.repositories;

import com.ansarul.WebAPI_PostGreSQL_Spring.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository <Franchise, Integer> {
}
