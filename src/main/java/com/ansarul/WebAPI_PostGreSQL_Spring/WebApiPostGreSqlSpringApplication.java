package com.ansarul.WebAPI_PostGreSQL_Spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebApiPostGreSqlSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebApiPostGreSqlSpringApplication.class, args);
	}

}
